#!/bin/usr/python3
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
import threading, time, json

def main():
	driver = startBrowser()
	#Terms is drop down menu for selecting terms 
	#and all_terms is a list of each option
	terms = driver.find_element_by_xpath('//select[@name="term"]')
	all_terms = terms.find_elements_by_tag_name("option")
	for term in all_terms[0:-1]:
		term = term.get_attribute('value')
		threading.Thread(target=threadedBrowser, args=(term, )).start()
	all_terms[-1].click()
	getClasses(driver)
	
def startBrowser():
	options = Options()
	options.set_headless(headless=True)
	driver = webdriver.Firefox(firefox_options=options)
	driver.get('https://ssb.avc.edu/AVCPROD/pw_pub_sched.p_Search')
	return driver

#finds an element by the value which looks like <tag value="string">
def findValue(driver, string, tag="*"):
	return driver.find_elements_by_xpath(
		'//{0}[@value="{1}"]'.format(tag, string))

#starts a browser for each term does the same as the main browser
def threadedBrowser(term):
	driver = startBrowser()
	findValue(driver, term)[0].click()
	getClasses(driver)
	
#gets a list of each table line "<tr>"
def getClasses(driver):
	#clicks the Select Term button to refresh the page on the term
	findValue(driver, 'Select Term')[0].click()
	
	#sets all options to all '%'
	for option in findValue(driver, '%', tag='option'):
		option.click()
	findValue(driver, 'Search')[0].click()
	#waits until the last element of the page is loaded 
	#which is a Begin New Search button and waits one more second
	WebDriverWait(driver, 60).until(
		EC.presence_of_element_located(
		(By.XPATH, '//input[@value="Begin New Search"]')))
	time.sleep(1)

	#scraps the term and table then splits the table by into rows <tr>
	term = ' '.join(driver.title.split(' ')[-2:])
	classes = driver.find_element_by_tag_name("table")
	html = classes.get_attribute('innerHTML')
	driver.close()
	soup = BeautifulSoup(html, 'lxml')
	classlist = soup.find_all('tr')
	makeJSON(term, classlist)

#formats each table line row a dictionary entry and stores it as JSON
#{term : {subject : [ {course: 'name' classes: [ {class attributes} ]}]}}
#lists are used instead of dictionaries when order matters
def makeJSON(term, classlist):
	JSON = {}
	JSON[term] = {}
	Jterm = JSON[term]
	for block_tag in classlist:
		try:
			class_tag = block_tag.find('td')['class'][0]
			text_tag = block_tag.find('td').text
		except KeyError:
			continue
		#Subjects are things like MATH or ENGL
		if class_tag == 'subject_header':
			Jterm[text_tag] = []
			Jsub = Jterm[text_tag]
		#course is specific classes like MATH 102 or ENGL 101
		elif class_tag == 'crn_header':
			Jsub.append({'course': text_tag, 'classes':[]})
			Jcourse = Jsub[-1]['classes']
		#determines if there is a crn number 
		elif block_tag.find_all('td')[1].text.replace(' ', '').isdigit():
			Jcourse.append(classparse(block_tag))
	f = open('json/{}.json'.format(term), 'w+')
	json.dump(JSON, f)
	f.close()
	return

#returns a dictionary for a class 
#the dictionary has specific entries that contain the class attributes
def classparse(block_tag):
	#each <td> is a part of the table row
	class_attr = block_tag.find_all('td')
	Jclass = {}
	Jclass['crn'] = class_attr[1].text
	Jclass['status'] = class_attr[0].text
	Jclass['sect'] = class_attr[2].text.replace('\n', '')
	Jclass['credits'] = class_attr[3].text
	weekdays = 'MTWRFSU'
	classdays = []
	#checks to see if it is a online class
	#if not it gets the days and meeting time
	if class_attr[6].text == 'TBA':
		classdays.append('TBA')
		Jclass['meeting time'] = 'TBA'
		normal_range = range(7, 14)
	else:
		for x in range(6, 13):
			if class_attr[x].text in weekdays:
				classdays.append(class_attr[x].text)
		Jclass['meeting time'] = class_attr[13].text
		normal_range = range(14, 21)
	#gets the rest of the attributes 
	#online classes have smaller numbers than normal classes
	Jclass['meeting days'] = classdays
	attrs = ['location', 'cap', 'act', 'rem', 'instructor', 'date', 'weeks']
	for x, y in zip(attrs, normal_range):
		Jclass[x] = class_attr[y].text
	return Jclass

if __name__ == '__main__':
	main()
