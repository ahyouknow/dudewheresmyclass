const alphabet = Array.prototype.slice.call(document.getElementsByClassName("alphabet"))
const subjects = Array.prototype.slice.call(document.getElementsByClassName("subjects"))
const courses  = Array.prototype.slice.call(document.getElementsByClassName("courses"))
const elements = alphabet.concat(subjects.concat(courses))
var   hidden   = []


const virtualKey = 229
const backspace  = 8
const enterKey   = 13
const oneKey     = 48
const zKey       = 90

function runSearch(keyCode){
	// determines if 1-z, backspace, enter, or a virtualkey is pressed
	if ((keyCode < oneKey || keyCode > zKey) && keyCode !== backspace && keyCode !== virtualKey && keyCode !== enterKey){ 
		return
	}

	input = searchbar.value.toUpperCase()

	// Shows all hidden elements
	if (input == ""){
		for (var x = 0; x < hidden.length; x++){
			hidden[x].style.display = ""
		}
		hidden = []
		return
	}
	var searches = input.split("&")
	var visible  = []
	hidden       = []
	for (var searchNumber = 0; searchNumber < searches.length; searchNumber++){
		visible.concat(hideElements(searches[searchNumber].trim(), visible))
	}
}

// beginning of the interation function, all children of the found element are shown
function hideElements(search, visible){
	try{
		var element  = getElementByName(search)
	} catch(err) {
		return []
	}
	var children = Array.from(element.children)
	for (var child = 0; child < children.length; child++){
		children[child].style.display = ""
		visible.push(children[child])
		hidden = hidden.filter(x => x !== children[child])
	}
	visible = hideSiblingElementsIter(element, visible)
	return visible
}

// iteration process, each sibling of the element is set to hidden and the process runs again to hide the siblings of the parent element until the parent is within the table
function hideSiblingElementsIter(element, visible){
	var parentElement = element.parentElement
	var children      = Array.from(parentElement.children)
	var siblings      = children.filter(child => child !== element)

	element.style.display = ""
	visible.push(element)
	hidden = hidden.filter(x => x !== element)
	for (var sibling = 0; sibling < siblings.length; sibling++){
		if (!visible.includes(siblings[sibling])){
			siblings[sibling].style.display = "none"
			hidden.push(siblings[sibling])
		}
	}
	if (parentElement.id !== "term"){
		hideSiblingElementsIter(parentElement, visible)
	}
	return visible
}

function getElementByName(name){
	var index = elements.findIndex((x) => x.id.startsWith(name))
	if (index == -1){
		throw 'not found'
	}
	return elements[index]
}

const searchbar = document.getElementById("searchid")
searchbar.addEventListener("keyup", function(){runSearch(event.keyCode)});
