var $rows = $('#table tr');
$('#search').keydown(function(event) {
	if (event.keyCode === 13){
    		var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    		$rows.show().filter(function() {
			var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
			return !~text.indexOf(val);
    		}).hide();
	};
});
