#!/usr/bin/env python3
import gethtml, threading, os
from bs4 import BeautifulSoup
from multiprocessing import Queue, Pool
from jinja2 import Environment, FileSystemLoader

def main():
    subjectSplit = """<tr>
<td colspan="23">&nbsp;</td>
</tr>"""
    header = """<tr>
<td class="column_header_left">Status</td>
<td class="column_header_center">CRN</td>
<td class="column_header_center"><abbr title="Section Number">Sect</abbr></td>
<td class="column_header_center">Cred</td>
<TD class="column_header_center">
  <span 
    onmouseover="return overlib('NOTE: Courses marked with the green &quot;$0&quot; symbol exclusively use digital textbooks that are free of charge to students.');"
    onmouseout="return nd();">ZCT
  </span>
</TD>
<td class="column_header_center"><abbr title="Material Fees">$</abbr></td>
<td colspan="8" class="column_header_center">Meeting Time</td>
<td class="column_header_center">Mode</td>
<td class="column_header_center">Location</td>
<td class="column_header_center">Cap</td>
<td class="column_header_center">Act</td>
<td class="column_header_center">Rem</td>
<td class="column_header_left">Instructor</td>
<td class="column_header_center">Date</td>
<td class="column_header_center">Weeks</td>
<td class="column_header_left"><abbr title="Buy your books and supplies online">Books</abbr></td>
</tr>"""
    end = """<tr>
<td align="left" colspan="22" bgcolor="#000000"><font color="WHITE">End of report</font></td>
</tr>"""
    termsDict = gethtml.run()
    sortedTerms = sortterm(list(termsDict.keys()))
    index = open('static/index.html', 'w+')
    index.write(
    """<!DOCTYPE html>
    <html lang="en-US">
        <head>
            <meta http-equiv="refresh" content="0; url={}">
            <meta charset="UTF-8">
        </head>
        <body>
        </body>
    </html>""".format(sortedTerms[0]))
    index.close()
    threads = []
    termNames = [ x.split("*")[0] for x in sortedTerms ] 
    [os.remove("static/"+f) for f in os.listdir("static") if os.path.isfile("static/"+f) and f != "index.html" and f not in termNames ]
    for term, classlist in termsDict.items():
    #    processesInit(term, classlist, subjectSplit, header, end)
        t = threading.Thread(target=processesInit, args=(term, classlist, sortedTerms, subjectSplit, header, end))
        t.start()
        threads.append(t)
    for thread in threads:
        thread.join()
       
def processesInit(term, classlist, sortedTerms, subjectSplit, header, end):
    term, termvalue = term.split("*")
    #model = termmodel.model()
    #table, labTable = model.getTable(term.lower())
    #crnList = model.session.query(table.crn).all()
    #crns = set()
    #[crns.add(crn[0]) for crn in crnList]
    stupidTable = """<table><tr><td>
<FORM ACTION="pw_pub_sched.p_Search" METHOD="post">
<INPUT TYPE="submit" VALUE="Begin New Search">
<input type="hidden" name="term" value="{}" />
<input type="hidden" name="p_menu2use" value="S" />
</form>
</td></tr></table>""".format(termvalue)
    endofreport = """<tr>
<td align="left" colspan="23" bgcolor="#000000"><font color="WHITE">End of report</font></td>
</tr>"""
    classlist = classlist.replace(stupidTable, "")
    classlist = classlist.replace(header, "")
    classlist = classlist.replace(end, "")
    classlist = classlist.replace(endofreport, "")
    subjects = classlist.split(subjectSplit)
    subjects.pop(0)
    subjects.pop(-1)
    scrap = scrapper()
    pool = Pool(processes=4)
    data = {'classes':{}, 'labs':{}}
    for subject in pool.map(scrap.subject, subjects):
        subjectName = list(subject.keys())[0]
        data['labs'] = {**data['labs'], **subject[subjectName]['labs']}
        del subject[subjectName]['labs']
        data['classes'] = {**data['classes'], **subject}
    env = Environment(loader=FileSystemLoader('templates'))
    template = env.get_template('classlist.html')
    sort = ('crn', 'sect', 'status', 'rem', 'act', 'cap', 'date', 'meetingdays', 'meetingtime','mode', 'location', 'instructor')
    labsort = ('date', 'meetingdays', 'meetingtime', 'mode', 'location')
    render = template.render(data=data, term=term, terms=sortedTerms,
        sort=sort, labsort=labsort, firstLetter=sorted(list(data['classes'].keys()))[0][0])
    render = ' '.join(render.split())
    File = open('static/'+term, 'w+')
    File.write(render)
    File.close()

class scrapper:
    def __init__(self):
        self.courseSplit = '<tr>\n<td colspan="20">&nbsp;</td>\n</tr>'
        self.courseSplit = BeautifulSoup(self.courseSplit, 'lxml').find_all('tr')[0]
        self.columns = ['status', 'crn', 'sect', 'cred', 'zct', 'fee',
                'meetingdays', 'meetingdays', 'meetingdays',
                'meetingdays', 'meetingdays', 'meetingdays',
                'nothing', 'meetingtime', 'mode', 'location',
                'cap', 'act', 'rem', 'instructor', 'date', 'weeks', 'books' ]
        self.classTemplate = {}
        for column in self.columns:
            self.classTemplate[column] = ''
        self.labcolumns = ['', '', '', '', '', '', 
                'meetingdays', 'meetingdays', 'meetingdays',
                'meetingdays', 'meetingdays', 'meetingdays',
                'nothing', 'meetingtime', 'mode', 'location'
                '', '', '','','', 'date', '', '' ]
        self.labTemplate = {}
        for column in self.labcolumns:
            self.labTemplate[column] = ''


    def subject(self, subject):
        subject = BeautifulSoup(subject, 'lxml').find_all('tr')
        subjectName = subject.pop(0)
        subjectName = subjectName.text.strip()
        q = Queue()
        courses = splitList(subject, self.courseSplit)
        threads = len(courses)
        completedThreads = 0
        for course in courses:
            threading.Thread(target=self.courseRun, args=(course, subjectName, q)).start()
        returndict = {'labs':{}}
        while completedThreads != threads:
            #print(subjectName+"\nthreads: "+str(threads)+"\ncompleted: "+str(completedThreads))
            data = q.get()
            courseName = list(data.keys())[0]
            returndict[courseName] = []
            for record in data[courseName]['classes']:
                returndict[courseName].append(dict(record))
            for crn in data[courseName]['labs'].keys():
                returndict['labs'][crn] = []
                for labclass in data[courseName]['labs'][crn]:
                    returndict['labs'][crn].append(dict(labclass))
            completedThreads+=1
        #print(subjectName+" finished")
        return {subjectName:returndict}

    def courseRun(self, course, subjectName, q):
        course = stripList(course, self.courseSplit)
        courseName = course.pop(0)
        courseName = courseName.text.strip()
        returndict = {'classes':set(), 'labs':{}}
        record = {'subject':subjectName, 'course':courseName}
        for Class in course:
            Class = Class.find_all('td')
            if Class[0].text.strip() == '':
                #lab
                labrecord = self.labTemplate.copy()
                position = 0
                for column in Class:
                    labrecord[self.labcolumns[position]] = labrecord[self.labcolumns[position]]+column.text.strip()+" "
                    if 'colspan' in column.attrs:
                        position+=int(column['colspan'])
                    else:
                        position+=1
                returndict['labs'][record['crn']].add(tuple(labrecord.items()))
            else:
                #class
                record = self.classTemplate.copy()
                position = 0
                for column in Class:
                    record[self.columns[position]] = record[self.columns[position]]+column.text.strip()+" "
                    if 'colspan' in column.attrs:
                        position+=int(column['colspan'])
                    else:
                        position+=1
                #print(record['mode'])
                record['crn'] = int(record['crn'].replace('(H)', '').strip())
                returndict['classes'].add(tuple(record.items()))
                returndict['labs'][record['crn']] = set()
        q.put({courseName:returndict})

def stripList(List, element):
    [List.remove(element) for _ in range(List.count(element))]
    return List

def splitList(List, element):
    indices = [x for x, ele in enumerate(List) if ele == element]
    return [List[x: y] for x, y in zip([0]+indices, indices+[None])]

def sortterm(terms):
    termdict = {}
    for term in terms:
        term, termvalue = term.split("*")
        termdict[int(termvalue)] = term
    sortedValues = list(termdict.keys())
    sortedValues.sort(reverse=True)
    sortedTerms = []
    for termvalue in sortedValues:
        sortedTerms.append(termdict[termvalue])
    return sortedTerms

if __name__ == '__main__':
    main()
