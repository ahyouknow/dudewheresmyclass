from sqlalchemy import create_engine, MetaData, Table, Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import Session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.instrumentation import unregister_class
from sqlite3 import dbapi2 as sqlite
from sqlalchemy.dialects.sqlite import CHAR, INTEGER, BOOLEAN

class model:
    def __init__(self):
        self.engine = create_engine('sqlite+pysqlite:///avc.db')
        self.session = Session(bind=self.engine)
        self.metadata = MetaData(bind=self.engine, reflect=True)
        self.Base = declarative_base(metadata=self.metadata)

    def createTable(self, name):
        table = Table(name, self.metadata,
        Column('crn', INTEGER, primary_key=True),
        Column('subject', CHAR, nullable=False),
        Column('course', CHAR, nullable=False),
        Column('status', CHAR, nullable=False),
        Column('sect', CHAR, nullable=False),
        Column('credits', CHAR, nullable=False),
        Column('booklink', CHAR, nullable=True),
        Column('meetingdays', CHAR, nullable=False),
        Column('meetingtime', CHAR, nullable=False),
        Column('location', CHAR, nullable=False),
        Column('cap', INTEGER, nullable=False),
        Column('act', INTEGER, nullable=False),
        Column('rem', INTEGER, nullable=False),
        Column('instructor', CHAR, nullable=False),
        Column('date', CHAR, nullable=False),
        Column('weeks', INTEGER, nullable=False),
        )
        labTable = Table(name+'_labs', self.metadata,
        Column('key', INTEGER, primary_key=True, autoincrement=True),
        Column('crn', INTEGER, ForeignKey(name+'.crn'), nullable=False),
        Column('meetingdays', CHAR, nullable=False),
        Column('meetingtime', CHAR, nullable=False),
        Column('location', CHAR, nullable=False),
        Column('date', CHAR, nullable=False),
        )
        self.metadata.create_all(self.engine)
        return table, labTable

    def dropTables(self):
        self.metadata.drop_all(self.engine)
    
    def getTable(self, name):
        if name in self.metadata.tables.keys():
            termTable = Table(name, self.metadata, autoload=True, autoload_with=self.engine)
            labTermTable = Table(name+'_labs', self.metadata, autoload=True, autoload_with=self.engine)
        else:
            termTable, labTermTable = self.createTable(name)
        class table(self.Base):
            __table__ = termTable

        class labTable(self.Base):
            __table__ = labTermTable
        return table, labTable

    def unregisterTable(self, table):
       unregister_class(table) 
        
