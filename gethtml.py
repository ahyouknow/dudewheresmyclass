from requests_futures.sessions import FuturesSession
from bs4 import BeautifulSoup

def run():
    session = FuturesSession()
    homepageReq = session.get('https://ssb.avc.edu/AVCPROD/pw_pub_sched.p_Search')
    data = {'term_desc': '', 'term': '', 'sel_subj': '%', 'sel_camp': '%', 'sel_instr': '%',
    'IsOpen': 'N', 'IsOnline': 'N', 'IsShrtTrm': 'N',  'sel_day': 'dummy', 'sel_schd': 'dummy', 
    'sel_ism': '%', 'sel_sess': 'dummy', 'sel_ptrm': '%', 'begin_hh': '5', 'begin_mi': '0',
    'begin_ap': 'a', 'end_hh': '11', 'end_mi': '0', 'end_ap': 'p'}
    returndict = {}
    requests = []
    terms = []
    homepage = homepageReq.result()
    soup = BeautifulSoup(homepage.text, 'lxml')
    for term in list(soup.find("select", attrs={"name":"term"}))[1:]:
        termName = term.text.strip()
        termData = data.copy()
        termData.update({'term_desc':termName, 'term':term['value']})
        requests.append(session.post('https://ssb.avc.edu/AVCPROD/pw_pub_sched.p_listthislist', data=termData))
        terms.append(termName+"*"+term['value'])
    for term, request in zip(terms, requests):
        classlist = request.result()
        returndict[term] = classlist.text
    return returndict

if __name__ == '__main__':
    test = run()
    print(type(test['Fall 2019']))
