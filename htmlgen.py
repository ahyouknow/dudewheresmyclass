#!/usr/bin/python3
import json, os

def main():
	for jfile in os.listdir('json/'):
		html = """<!doctype html><html>
<link rel=\"stylesheet\" href=\"de.css\">
<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-1.7.1.min.js\"></script>
<body><input type=\"text\" id=\"search\" placeholder=\"Enter to search\">
<div>
<table id=\"table\">"""
		jsonfile = open('json/'+jfile, 'r')
		data = json.load(jsonfile)
		jsonfile.close()
		term = jfile.replace(".json", "")
		html+=subjectparse(data[term])
		html+="""\n</table>\n</div>\n</body>
<script src=\"search.js\"></script>
</html>"""
		htmlfile = open(term+".html", 'w+')
		htmlfile.write(html)
		htmlfile.close()

def subjectparse(subjects):
	output = ''
	for subject in sorted(list(subjects.keys())):
		output+="\n<tr>{}\n</tr>".format(colmake(subject, htmlclass='subject', colspan='20'))
		output+=courseparse(subjects[subject], subject)
	return output

def courseparse(courses, subject):
	output = ''
	headerformat1 = [('crn','medium'), ('sect','small'), ('credits','small'), ('weeks','small'), ('date','medium')]
	headerformat2 = [('meeting time','large'), ('location','big'), ('status','medium'), ('cap','small'), ('act','small'), ('rem','small'), ('instructor','big')]
	for course in courses:
		classes, attributes = classparse(course['classes'], headerformat1, headerformat2, course['course'], subject)
		hidden = '\n<td style=\"display:none;\">{}</td>'.format(' '.join(attributes)+' '+subject)
		output+="\n<tr>{0}{1}\n</tr>".format(colmake(course['course'], htmlclass='course', colspan='20'), hidden)
		output+="\n<tr>{0}{1}\n</tr>".format(headercreate(headerformat1, headerformat2, course['course']), hidden)
		output+=classes
	return output

def headercreate(headerformat1, headerformat2, course):
	output = ''
	for header in headerformat1:
		output+=colmake(header[0], htmlclass=header[1]+'header')
	output+=colmake('meetingdays', htmlclass='largeheader', colspan='7')
	for header in headerformat2:
		output+=colmake(header[0], htmlclass=header[1]+'header')
	output+='\n<td style=\"display:none;\">{0}</td>'.format(course)
	return output

def classparse(classes, rowformat1, rowformat2, course, subject):
	output = ''
	course_attri = set()
	for Class in classes:
		output+="\n<tr>"
		for column in rowformat1:
			output+=colmake(Class[column[0]], htmlclass=column[1]+'class')
			course_attri.add(Class[column[0]])
		if 'TBA' in Class['meeting days']:
			output+=colmake('TBA', htmlclass='largeclass', colspan='7')
		else:
			output+=daysmake(Class['meeting days'])
		for column in rowformat2:
			output+=colmake(Class[column[0]], htmlclass=column[1]+'class')
			course_attri.add(Class[column[0]])
		output+="\n<td style=\"display:none;\">{0} {1}</td>".format(course, subject)
		output+="\n</tr>"
	return output, course_attri

def daysmake(meetingdays):
	output = ''
	days = 'MTWRFSU'
	for day in days:
		if day in meetingdays:
			output+=colmake(day, htmlclass='smallclass')
		else:
			output+=colmake(' ', htmlclass='smallclass')
	return output


def colmake(text, htmlclass=False, colspan=False):
	output = '\n<td'
	if htmlclass:
	 	output+=" class=\"{}\"".format(htmlclass)	
	if colspan:
		output+=" colspan={}".format(colspan)
	output+=">{}</td>".format(text)
	return output

if __name__ == '__main__':
	main()
